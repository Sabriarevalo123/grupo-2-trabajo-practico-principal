import tensorflow as tf
from pathlib import Path
from tensorflow.keras.models import load_model
import HeartImg_Prep
import keras
import matplotlib.pyplot as plt

##################### PARÁMETROS #####################

size = 224
count_per_cat_train = 2000
cont_per_cat_test = (count_per_cat_train * 20) // 100
num_batch = HeartImg_Prep.num_batch

############# PREPARACIÓN IMÁGENES #############

# Cargar datos de entrenamiento
train_directory = Path("./src/train")
dataframe_train = HeartImg_Prep.load_data(train_directory, count_per_cat_train)

# Cargar datos de test
val_directory = Path("./src/test")
dataframe_val = HeartImg_Prep.load_data(val_directory, cont_per_cat_test)

# Obtener muestras aleatorias para entrenamiento y test
dataframe_train = HeartImg_Prep.obtener_muestras_aleatorias(
    dataframe_train, count_per_cat_train
)
dataframe_val = HeartImg_Prep.obtener_muestras_aleatorias(
    dataframe_val, cont_per_cat_test
)

# Preparación de imágenes para train y test
train_images = HeartImg_Prep.prepare_image_generator_ge(dataframe_train, "training") # Entrenamiento
val_images = HeartImg_Prep.prepare_image_generator_ge(dataframe_val, "validation") # Validación

##################### MODELO PREENTRNADO #####################

# Cargar modelo preentrenado (cambiar por el modelo deseado)
model = load_model("./src/models/Best_CNN.h5")

# Número de capas en el modelo (OPCIONAL)
# num_layers = len(model.layers)
# print("Número de capas en el modelo:", num_layers)

#n = 4  # Número de capas que se descongelarán
#for layer in model.layers[-n:]:
#    layer.trainable = True  # Descongelar las últimas n capas

##################### FINE TUNNING #####################

# Compilación del modelo
model.compile(
    loss="categorical_crossentropy",
    optimizer=tf.keras.optimizers.Adam(learning_rate=1e-4),  # Aprendizaje más lento
    metrics=[
        tf.keras.metrics.CategoricalAccuracy(),
        tf.keras.metrics.Precision(),
        tf.keras.metrics.AUC(
            num_thresholds=200,
            curve="ROC",
            summation_method="interpolation",
            multi_label=False,
        ),
    ],
)

# Guardar el mejor modelo según su categorical accuracy (precisión para cada categoría)
checkpoint = keras.callbacks.ModelCheckpoint(
    filepath="./src/models/Best_FineTunning.h5",
    save_weights_only=False,
    monitor="categorical_accuracy",
    mode="max",
    save_best_only=True,
    verbose=1,
)

# Ajustes en el entrenamiento
result = model.fit(
    train_images,
    validation_data=val_images,
    epochs=10, # Numero de iteraciones por epoca
    callbacks=[checkpoint],
)


##################### GRÁFICOS #####################
import matplotlib.pyplot as plt

# Obtener métricas del historial
categorical_accuracy = result.history['categorical_accuracy']
precision = result.history['precision']
auc = result.history['auc']

# Crear gráfico
epochs = range(1, len(categorical_accuracy) + 1)

plt.figure(figsize=(12, 4))
plt.subplot(1, 3, 1)
plt.plot(epochs, categorical_accuracy, 'b', label='Categorical Accuracy')
plt.title('Categorical Accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend()

plt.subplot(1, 3, 2)
plt.plot(epochs, precision, 'r', label='Precision')
plt.title('Precision')
plt.xlabel('Epochs')
plt.ylabel('Precision')
plt.legend()

plt.subplot(1, 3, 3)
plt.plot(epochs, auc, 'g', label='AUC')
plt.title('AUC')
plt.xlabel('Epochs')
plt.ylabel('AUC')
plt.legend()

plt.tight_layout()
plt.show()
