import random
import cv2
import numpy as np
import os

# Definir la ruta del directorio de entrada y salida
input_dir = './src/input'
output_dir = './src/output'

# Definir la dimensión de la imagen reducida
new_size = (224, 224)

# Definir la cantidad de ruido
noise_amount = 0.5

# Iterar a través de cada archivo en el directorio de entrada
for filename in os.listdir(input_dir):
    # Cargar la imagen
    img = cv2.imread(os.path.join(input_dir, filename))

    # Reducir la dimensión de la imagen
    img_resized = cv2.resize(img, new_size)

    # Generar un ángulo de forma aleatoria (negativo)
    angle = random.uniform(-5, 5)  # Rango de ángulos negativos

    # Rotar la imagen
    rows, cols, _ = img_resized.shape
    M = cv2.getRotationMatrix2D((cols/2, rows/2), angle, 1)
    img_rotated = cv2.warpAffine(img_resized, M, (cols, rows))

    # Añadir ruido a la imagen
    noise = np.random.normal(0, noise_amount, img_rotated.shape)
    img_noisy = img_rotated + noise

    # Guardar la imagen procesada en el directorio de salida
    cv2.imwrite(os.path.join(output_dir, filename), img_noisy)

print('Procesamiento de imágenes completado.')