# Documentación del Código - Modelo Inceptión para Clasificación de Imágenes Cardíacas

Este código implementa un modelo de red neuronal convolucional (CNN) utilizando TensorFlow y Keras para clasificar imágenes cardíacas en las siguientes 6 categorías:

- N:	Latido normal
- S:	Latido prematuro supraventricular
- V:	Contracción ventricular prematura
- F:	Fusión de latido ventricular y latido normal
- Q:	Latido no clasificable
- M:	Infarto de miocardio

Este modelo en particular implementa InceptionV3, un modelo de CNN desarrollado por Google cuyo objetivo es asistir en el analisis de imágenes y detección de objetos. Permite crear redes más profundas sin que la cantidad de parametros crezca demasiado.

## Librerías Importadas

- `os`: Proporciona funciones para interactuar con el sistema operativo, utilizado para deshabilitar MKL.
- `Path` de `pathlib`: Utilizado para trabajar con rutas de archivos y directorios.
- `tensorflow` y `tensorflow.keras`: Librerías para trabajar con redes neuronales y modelos.
- `HeartImg_Prep`: Un módulo personalizado que contiene funciones para la preparación de datos de imágenes cardíacas.

## Parámetros

- `size`: Tamaño de las imágenes.
- `count_per_cat_train`: Cantidad de imágenes para el entrenamiento que se toman por categoría. La idea es poder balancear el número de imágenes de cada una de las etiquetas.
- `cont_per_cat_test`: Número de imágenes por categoría para vaidación del modelo (20% de `count_per_cat_train`).

## Preparación de Imágenes

Se cargan y preparan los datos de entrenamiento y prueba utilizando el módulo `HeartImg_Prep`. En dicho módulo se cargan las imágenes que utilizan como input:

- Se utilizan las imágenes de entrenamiento de la carpeta `/src/train`
- Se utilizan las imágenes de para pruebas y test de la carpeta `/src/test`

A su vez, se procesan las imágenes para transformarlas en numpy arrays tridimensionales. Donde se representa la altura, el ancho y los canales de la imagen de forma numérica lo que lo hace compatible con las operaciones y cálculos que se realizan en modelos de aprendizaje automático.

## Modelo Inception

Primero se crean las capas de Inception:
- Se ingresa como input el tamaño de la imágen. Pasandole los tres canales de color RGB.
- No se incluye las capas que son completamente conectadas.
- Se cargan pesos pre-asignados con "imagenet". El estandar para Keras.

Cabe resaltar que Inception no permite que se analicen las imágenes en blanco y negro, por lo tanto se debe analizar con RGB.

Para evitar el futuro entrenamiento de estas capas se asignan como no entrenables.

Se crea el modelo que se utilizará con las siguientes capas:
- Las capas de Inception creadas anteriormente.
- La capa de aplanamiento, para convertir las matrices de información en una columna con todos los datos.
- 3 capas de Dense, que son completamente conectadas.
- Dos capas de Dropout, que elimina nodos con el objetivo de evitar el overfitting y asegurarse de que no hay nodos codependientes entre sí.
- La ultima capa de Dense, con una salida de 6 neuronas para las 6 categorias.

## Compilación del Modelo

El modelo se compila utilizando la función de pérdida `categorical_crossentropy`(entropía cruzada categórica), comúnmente usada para problemas de clasificación con múltiples clases. La misma mide la discrepancia entre las predicciones del modelo y las etiquetas reales.

Se elige como optimizador `Adam` con una tasa de aprendizaje de 1e-4. Un optimizador en el contexto del aprendizaje automático es un algoritmo que determina cómo los pesos del modelo deben ser ajustados para mejorar el aprendizaje. La tasa 1e-4, es la tasa de actualización de los pesos del modelo. Esta es una tasa menor a la habitual para mitigar aún más el sobreajuste. Además, se agregan las siguientes métricas de evaluación:

-  Se agrega la métrica de `categorical_accuracy` para evaluar la precisión del modelo en términos de clasificación categórica.
- `Precision` para evaluar la precisión del modelo en la predicción positiva de las clases.
- `Area under the ROC Curve (AUC)` para evaluar la capacidad de clasificación del modelo.

## Guardado del Mejor Modelo

El modelo se guarda en un archivo h5 utilizando la la función `checkpoint`. Este método establece los parametros de guardado, incluyendo la ubicación y tratamiento de capas. Además, selecciona el mejor modelo comparando la metrica categorical_accuracy, quedanse siempre con la de mayor rendimiento.

## Entrenamiento del Modelo

El modelo se entrena con los datos de entrenamiento y validación, utilizando un total de 30 épocas (epochs). Al final de cada época, se llama a la función checkpoint para guardar el modelo si es que tuvo un rendimiento mayor.

---

**NOTA**: Este documento proporciona una descripción general de la estructura y funcionamiento del código. Para obtener detalles más específicos sobre cada parte del código, se recomienda revisar los comentarios incluidos en el mismo.
