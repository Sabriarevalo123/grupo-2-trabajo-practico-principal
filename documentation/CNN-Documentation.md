# Documentación del Código - Modelo CNN para Clasificación de Imágenes Cardíacas

Este código implementa un modelo de red neuronal convolucional (CNN) utilizando TensorFlow y Keras para clasificar imágenes cardíacas en las siguientes 6 categorías:

- N:	Latido normal
- S:	Latido prematuro supraventricular
- V:	Contracción ventricular prematura
- F:	Fusión de latido ventricular y latido normal
- Q:	Latido no clasificable
- M:	Infarto de miocardio

Se siguen varias etapas, incluyendo la preparación de datos, creación del modelo y entrenamiento del mismo.

## Librerías Importadas

- `os`: Proporciona funciones para interactuar con el sistema operativo, utilizado para deshabilitar MKL.
- `Path` de `pathlib`: Utilizado para trabajar con rutas de archivos y directorios.
- `tensorflow` y `tensorflow.keras`: Librerías para trabajar con redes neuronales y modelos.
- `HeartImg_Prep`: Un módulo personalizado que contiene funciones para la preparación de datos de imágenes cardíacas.

## Deshabilitar MKL para Evitar Errores de Memoria

Se deshabilita MKL (Math Kernel Library) para evitar errores de memoria.

## Parámetros

- `size`: Tamaño de las imágenes.
- `count_per_cat_train`: Cantidad de imágenes para el entrenamiento que se toman por categoría. La idea es poder balancear el número de imágenes de cada una de las etiquetas.
- `cont_per_cat_test`: Número de imágenes por categoría para vaidación del modelo (20% de `count_per_cat_train`).

## Preparación de Imágenes

Se cargan y preparan los datos de entrenamiento y prueba utilizando el módulo `HeartImg_Prep`. En dicho módulo se cargan las imágenes que utilizan como input:

- Se utilizan las imágenes de entrenamiento de la carpeta `/src/train`
- Se utilizan las imágenes de para pruebas y test de la carpeta `/src/test`

A su vez, se procesan las imágenes para transformarlas en numpy arrays tridimensionales. Donde se representa la altura, el ancho y los canales de la imagen de forma numérica lo que lo hace compatible con las operaciones y cálculos que se realizan en modelos de aprendizaje automático.

## Modelo CNN Personalizado

El modelo CNN consta de varias `capas convolucionales (Conv2D)`, `capas de pooling(MaxPooling2D)`, `capas de aplanamiento (Flatten)`, `capas completamente conectadas(Dense)` y `capas de Dropout`.

- 2 Capas convolucionales y de activación.
- 2 Capas de pooling para reducir la dimensionalidad.
- 1 Capa de aplanamiento.
- 3 Capas completamente conectadas, 2 de ellas con regularización L2 de 0.1.
- 2 Capas de dropout del 0.5.
- 1 Capa de salida con activación softmax para las 6 categorías.

En esta etapa se toman dos estrategias para poder compensar el sobreajuste:

- Regularización L2: agrega una penalización a los pesos del modelo, limitando su magnitud y favoreciendo modelos más simples.
- Capas de doupout: desactiva aleatoriamente un porcentaje de neuronas durante el entrenamiento, reduciendo así la interdependencia y la especialización excesiva de las neuronas.

En la primera Conv2D, determina las características de las imágenes que recibe como input a través de `input_shape=(224, 224, 1)`. Lo determina que las imágenes que recibe de entrenamiento el modelo son de 224px de alto, 224 de ancho y de 1 canal, es decir, en escala de grises.

## Compilación del Modelo

El modelo se compila utilizando la función de pérdida `categorical_crossentropy`(entropía cruzada categórica), comúnmente usada para problemas de clasificación con múltiples clases. La misma mide la discrepancia entre las predicciones del modelo y las etiquetas reales.

Se elige como optimizador `Adam` con una tasa de aprendizaje de 1e-4. Un optimizador en el contexto del aprendizaje automático es un algoritmo que determina cómo los pesos del modelo deben ser ajustados para mejorar el aprendizaje. La tasa 1e-4, es la tasa de actualización de los pesos del modelo. Esta es una tasa menor a la habitual para mitigar aún más el sobreajuste. Además, se agregan las siguientes métricas de evaluación:

-  Se agrega la métrica de `categorical_accuracy` para evaluar la precisión del modelo en términos de clasificación categórica.
- `Precision` para evaluar la precisión del modelo en la predicción positiva de las clases.
- `Area under the ROC Curve (AUC)` para evaluar la capacidad de clasificación del modelo.

## Guardado del Mejor Modelo

El modelo se guarda en un archivo h5 utilizando la la función `checkpoint`. Este método establece los parametros de guardado, incluyendo la ubicación y tratamiento de capas. Además, selecciona el mejor modelo comparando la metrica categorical_accuracy, quedanse siempre con la de mayor rendimiento.

## Entrenamiento del Modelo

El modelo se entrena con los datos de entrenamiento y validación, utilizando un total de 30 épocas. Al final de cada época, se llama a la función checkpoint para guardar el modelo si es que tuvo un rendimiento mayor.

---

**NOTA**: Este documento proporciona una descripción general de la estructura y funcionamiento del código. Para obtener detalles más específicos sobre cada parte del código, se recomienda revisar los comentarios incluidos en el mismo.
